// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class SingleStatistics {
  bool isLong;
  double amount;
  double singleRR;
  bool isAWin;

  SingleStatistics({
    required this.isLong,
    required this.amount,
    required this.singleRR,
    required this.isAWin,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'isLong': isLong,
      'amount': amount,
      'singleRR': singleRR,
      'isAWin': isAWin,
    };
  }

  factory SingleStatistics.fromMap(Map<String, dynamic> map) {
    return SingleStatistics(
      isLong: map['isLong'] as bool,
      amount: map['amount'] as double,
      singleRR: map['singleRR'] as double,
      isAWin: map['isAWin'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory SingleStatistics.fromJson(String source) =>
      SingleStatistics.fromMap(json.decode(source) as Map<String, dynamic>);
}
