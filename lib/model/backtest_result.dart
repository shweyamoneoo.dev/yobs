class BacktestResult {
  int? count;
  String? averageRR;
  String? totalProfit;
  double? largestProfit;
  String? winRate;
  int? winCount;
  int? lossCount;

  BacktestResult({
    this.count,
    this.averageRR,
    this.totalProfit,
    this.largestProfit,
    this.winRate,
    this.winCount,
    this.lossCount,
  });
}
