// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:yobs/model/single_statistics.dart';

class BacktestStatistics {
  int id;
  String title;
  double accountSize;
  String pair;
  List<SingleStatistics> statistics;
  BacktestStatistics({
    required this.id,
    required this.title,
    required this.accountSize,
    required this.pair,
    required this.statistics,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'title': title,
      'accountSize': accountSize,
      'pair': pair,
      'statistics': statistics.map((x) => x.toMap()).toList(),
    };
  }

  factory BacktestStatistics.fromMap(Map<String, dynamic> map) {
    return BacktestStatistics(
      id: map['id'] as int,
      title: map['title'] as String,
      accountSize: map['accountSize'] as double,
      pair: map['pair'] as String,
      statistics: List<SingleStatistics>.from(
        (map['statistics'] as List<dynamic>).map<SingleStatistics>(
          (x) => SingleStatistics.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory BacktestStatistics.fromJson(String source) =>
      BacktestStatistics.fromMap(json.decode(source));

  static List<String> toJsonList(List<BacktestStatistics> list) {
    List<String> strList = [];
    for (BacktestStatistics bs in list) {
      strList.add(bs.toJson());
    }
    return strList;
  }

  static List<BacktestStatistics> toObjectList(List<String> jsonList) {
    List<BacktestStatistics> bsList = [];
    for (String json in jsonList) {
      bsList.add(BacktestStatistics.fromJson(json));
    }
    return bsList;
  }
}
