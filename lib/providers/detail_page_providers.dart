import 'package:hooks_riverpod/hooks_riverpod.dart';

final winOrLoseCheckboxProvider = StateProvider<bool>((ref) {
  return true;
});

final longOrShortCheckboxProvider = StateProvider<bool>((ref) {
  return true;
});

final showChartProvider = StateProvider<bool>((ref) {
  return true;
});

final maxYProvider = StateProvider<double>((ref) {
  return 0.0;
});
