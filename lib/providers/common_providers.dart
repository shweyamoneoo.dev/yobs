import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yobs/utils/preference_utils.dart';

import '../model/backtest_statistics.dart';

final backtestStatisticsListProvider =
    StateProvider<List<BacktestStatistics>>((ref) {
  final prefUtils = ref.watch(preferenceUtils);
  List<String> strList = prefUtils.getStatisticsStrList() ?? [];
  return BacktestStatistics.toObjectList(strList);
});

final sharedPreferencesProvider = Provider<SharedPreferences>((_) {
  return throw UnimplementedError();
});

final preferenceUtils = StateProvider<PreferenceUtils>((ref) {
  final pref = ref.watch(sharedPreferencesProvider);
  return PreferenceUtils(sharedPreferences: pref);
});
