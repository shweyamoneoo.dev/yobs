import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:yobs/providers/common_providers.dart';
import 'package:yobs/widgets/add_title_dialog.dart';
import 'package:yobs/widgets/title_list_card.dart';

import '../utils/app_colors.dart';

class HomeScreen extends HookConsumerWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bsList = ref.watch(backtestStatisticsListProvider);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('YOBS'),
          centerTitle: true,
          scrolledUnderElevation: 0.0,
          foregroundColor: AppColors.text200,
        ),
        body: ListView.builder(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8).dm,
          itemCount: bsList.length,
          itemBuilder: (context, index) {
            return TitleListCard(
              bs: bsList[index],
              index: index,
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: AppColors.primary800,
          onPressed: () {
            showDialog(
              context: context,
              builder: (context) => const AddTitleDialog(),
            );
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
