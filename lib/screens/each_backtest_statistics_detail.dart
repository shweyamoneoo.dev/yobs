import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:yobs/model/single_statistics.dart';
import 'package:yobs/widgets/add_statistics_card.dart';
import 'package:yobs/widgets/chart_widget.dart';
import 'package:yobs/widgets/statistics_list_item.dart';

import '../providers/common_providers.dart';
import '../providers/detail_page_providers.dart';
import '../utils/app_colors.dart';

class BacktestStatisticsDetailScreen extends HookConsumerWidget {
  final int currentIndex;
  const BacktestStatisticsDetailScreen({required this.currentIndex, super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final bsList = ref.watch(backtestStatisticsListProvider);
    final showChart = ref.watch(showChartProvider);
    List<SingleStatistics> statistics = bsList[currentIndex].statistics;
    ScrollController controller = useScrollController();
    controller.addListener(() {
      if (controller.position.pixels * 8 >= ScreenUtil().screenHeight) {
        if (showChart) {
          ref.read(showChartProvider.notifier).state = false;
        }
      }
    });
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            scrolledUnderElevation: 0,
            backgroundColor: Colors.transparent,
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0).dm,
            child: Column(
              children: [
                Stack(
                  children: [
                    Column(
                      children: [
                        Card(
                          color: AppColors.primary800,
                          elevation: 2,
                          child: Column(
                            children: [
                              AddStatisticsCard(currentIndex),
                              // if (showChart)
                              ChartWidget(
                                bs: bsList[currentIndex],
                              )
                                  .animate(target: showChart ? 0 : 1)
                                  .blur(duration: 400.ms)
                                  .swap(
                                    builder: (context, child) =>
                                        const SizedBox(),
                                  ),
                            ],
                          ),
                        ),
                        12.verticalSpace
                      ],
                    ),
                    if (!showChart)
                      Positioned(
                        bottom: -4.h,
                        right: 4,
                        child: IconButton(
                            onPressed: () {
                              ref.read(showChartProvider.notifier).state = true;
                            },
                            icon: DecoratedBox(
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  shape: BoxShape.circle,
                                  color: AppColors.primary800),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0).dg,
                                child: const Icon(
                                  Icons.show_chart,
                                  color: Colors.cyan,
                                ),
                              ),
                            ).animate().shimmer()),
                      )
                  ],
                ),
                Flexible(
                  child: ListView.separated(
                    controller: controller,
                    padding: const EdgeInsets.only(bottom: 16).dm,
                    separatorBuilder: (context, index) {
                      return const Divider();
                    },
                    shrinkWrap: true,
                    itemCount: statistics.length,
                    itemBuilder: (context, index) => StatisticsListItem(
                      statistics: statistics[index],
                      stIndex: index,
                      index: currentIndex,
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
