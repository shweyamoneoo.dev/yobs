import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:yobs/model/backtest_statistics.dart';

import '../utils/functions.dart';

class ChartWidget extends StatefulWidget {
  final BacktestStatistics bs;
  const ChartWidget({super.key, required this.bs});

  @override
  State<ChartWidget> createState() => _LineChartSample2State();
}

class _LineChartSample2State extends State<ChartWidget> {
  List<Color> gradientColors = [
    Colors.cyan,
    Colors.blue,
  ];

  bool showAvg = false;
  double maxX = 10;
  double maxY = 100;
  double maxYValue = 100;
  double ySegmentValue = 10;
  num divideBy = 1;

  @override
  Widget build(BuildContext context) {
    if (widget.bs.statistics.length >= 10) {
      maxX = widget.bs.statistics.length + 1;
    }
    maxYValue = widget.bs.accountSize > largestProfit(widget.bs)
        ? widget.bs.accountSize
        : largestProfit(widget.bs) * 1.5;

    if (maxYValue > 100) {
      divideBy = (pow(10, maxYValue.toInt().toString().length - 2));
      maxY = maxYValue / divideBy;
    }
    ySegmentValue = (maxY / 5).floorToDouble();
    // if (double.tryParse(totalProfit(widget.bs)) != null) {
    //   if (double.parse(totalProfit(widget.bs)) / 1000 >= 100) {
    //     maxY = double.parse(totalProfit(widget.bs)) * 1.5;
    //   }
    // }

    return Column(
      children: [
        8.verticalSpace,
        Row(
          children: [
            12.horizontalSpace,
            const Expanded(
              child: Divider(),
            ),
            6.horizontalSpace,
            Text(
              'Chart',
              style: TextStyle(fontSize: 14.sp),
            ),
            6.horizontalSpace,
            const Expanded(child: Divider()),
            12.horizontalSpace
          ],
        ),
        2.verticalSpace,
        Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0).dm,
            child: Text(
              'N = $divideBy',
              textAlign: TextAlign.right,
              style: TextStyle(
                fontSize: 14.sp,
              ),
            ),
          ),
        ),
        AspectRatio(
          aspectRatio: 1.70,
          child: Padding(
            padding: const EdgeInsets.only(
              right: 18,
              left: 12,
              top: 6,
              bottom: 12,
            ),
            child: LineChart(
              mainData(),
            ),
          ),
        ),
      ],
    );
  }

  Widget bottomTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
    Widget text = const Text('', style: style);
    if (value.toInt() % 2 == 0) {
      text = Text(value.toInt().toString(), style: style);
    }
    return SideTitleWidget(
      axisSide: meta.axisSide,
      child: text,
    );
  }

  Widget leftTitleWidgets(double value, TitleMeta meta) {
    const style = TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 15,
    );
    String text = '';
    if (value != 0 && value % ySegmentValue == 0) {
      text = '${(value).toInt()}N';
    }

    return Padding(
      padding: const EdgeInsets.only(right: 6.0).dm,
      child: Text(text, style: style, textAlign: TextAlign.right),
    );
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        drawHorizontalLine: false,
        horizontalInterval: 10,
        verticalInterval: 1,
        // getDrawingHorizontalLine: (value) {
        //   return const FlLine(
        //     color: Colors.grey,
        //     strokeWidth: 1,
        //   );
        // },
        getDrawingVerticalLine: (value) {
          return const FlLine(
            color: Colors.grey,
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        rightTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        topTitles: const AxisTitles(
          sideTitles: SideTitles(showTitles: false),
        ),
        bottomTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            reservedSize: 30,
            interval: 1,
            getTitlesWidget: bottomTitleWidgets,
          ),
        ),
        leftTitles: AxisTitles(
          sideTitles: SideTitles(
            showTitles: true,
            interval: 1,
            getTitlesWidget: leftTitleWidgets,
            reservedSize: 42,
          ),
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: Border.all(color: const Color(0xff37434d)),
      ),
      minX: 0,
      maxX: maxX,
      minY: 0,
      maxY: maxY,
      lineBarsData: [
        LineChartBarData(
          spots: profitAddingList(widget.bs, divideBy),
          isCurved: true,
          gradient: LinearGradient(
            colors: gradientColors,
          ),
          barWidth: 4,
          isStrokeCapRound: true,
          dotData: const FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradient: LinearGradient(
              colors: gradientColors
                  .map((color) => color.withOpacity(0.3))
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }
}
