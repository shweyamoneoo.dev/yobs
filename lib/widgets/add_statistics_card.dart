import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:yobs/utils/app_consts.dart';
import 'package:yobs/widgets/win_loss_switch.dart';

import '../model/backtest_statistics.dart';
import '../model/single_statistics.dart';
import '../providers/common_providers.dart';
import '../providers/detail_page_providers.dart';
import 'long_short_switch.dart';

class AddStatisticsCard extends HookConsumerWidget {
  final int index;
  const AddStatisticsCard(this.index, {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    TextEditingController amount = useTextEditingController();
    TextEditingController singleRR = useTextEditingController();
    return Padding(
      padding:
          const EdgeInsets.only(left: 12.0, right: 12.0, top: 12.0, bottom: 12)
              .dg,
      child: Column(
        children: [
          4.verticalSpace,
          Row(
            children: [
              Expanded(
                flex: 1,
                child: SizedBox(
                  height: 32.h,
                  child: TextField(
                    controller: singleRR,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(AppConsts.doubleOnly)
                    ],
                    decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8)
                            .dm,
                        border: const OutlineInputBorder(),
                        hintText: 'Single RR'),
                  ),
                ),
              ),
              12.horizontalSpace,
              Expanded(
                flex: 1,
                child: SizedBox(
                  height: 32.h,
                  child: TextField(
                    controller: amount,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(AppConsts.doubleOnly)
                    ],
                    decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                                horizontal: 8, vertical: 8)
                            .dm,
                        border: const OutlineInputBorder(),
                        hintText: 'Amount (%)'),
                  ),
                ),
              ),
            ],
          ),
          12.verticalSpace,
          Row(
            children: [
              Flexible(
                flex: 1,
                child: Align(
                  alignment: Alignment.center,
                  child: LongShortSwitch(
                    isLong: ref.watch(longOrShortCheckboxProvider),
                    onTap: () {
                      ref.read(longOrShortCheckboxProvider.notifier).state =
                          !ref.read(longOrShortCheckboxProvider.notifier).state;
                    },
                  ),
                ),
              ),
              12.horizontalSpace,
              Flexible(
                flex: 1,
                child: Align(
                  alignment: Alignment.center,
                  child: WinLossSwitch(
                    isAWin: ref.watch(winOrLoseCheckboxProvider),
                    onTap: () {
                      ref.read(winOrLoseCheckboxProvider.notifier).state =
                          !ref.read(winOrLoseCheckboxProvider.notifier).state;
                    },
                  ),
                ),
              ),
            ],
          ),
          12.verticalSpace,
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              fixedSize: Size(0.7.sw, 36.h), // specify width, height
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.purple.shade100),
                borderRadius: BorderRadius.circular(
                  8.r,
                ),
              ),
            ),
            onPressed: () async {
              if (amount.text == '' || singleRR.text == '') {
                return;
              }
              // int currentIndex = ref.read(backtestStatisticsListProvider.notifier).state[index].statistics!.length;
              ref
                  .read(backtestStatisticsListProvider.notifier)
                  .state[index]
                  .statistics
                  .add(
                    SingleStatistics(
                        isLong: ref
                            .read(longOrShortCheckboxProvider.notifier)
                            .state,
                        amount: double.parse(amount.text),
                        singleRR: double.parse(singleRR.text),
                        isAWin:
                            ref.read(winOrLoseCheckboxProvider.notifier).state),
                  );
              ref.read(backtestStatisticsListProvider.notifier).state = [
                ...ref.read(backtestStatisticsListProvider)
              ];
              final prefUtils = ref.read(preferenceUtils.notifier).state;
              await prefUtils.saveStatisticsStrList(
                BacktestStatistics.toJsonList(
                    ref.read(backtestStatisticsListProvider.notifier).state),
              );

              amount.text = singleRR.text = '';
              ref.refresh(winOrLoseCheckboxProvider.notifier).state;
              ref.refresh(longOrShortCheckboxProvider.notifier).state;
            },
            child: Text(
              "Add",
              style: TextStyle(fontSize: 13.sp, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
