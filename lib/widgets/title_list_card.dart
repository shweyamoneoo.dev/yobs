import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:yobs/model/backtest_statistics.dart';
import 'package:yobs/providers/detail_page_providers.dart';
import 'package:yobs/screens/each_backtest_statistics_detail.dart';
import 'package:yobs/utils/functions.dart';

import '../model/backtest_result.dart';
import '../providers/common_providers.dart';
import '../utils/app_colors.dart';
import '../utils/app_consts.dart';
import 'add_title_dialog.dart';
import 'delete_confirm_dialog.dart';

class TitleListCard extends ConsumerWidget {
  final BacktestStatistics bs;
  final int index;
  const TitleListCard({super.key, required this.bs, required this.index});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    BacktestResult result = processStatistics(bs);
    return Stack(
      children: [
        Column(
          children: [
            10.h.verticalSpace,
            Card(
              color: AppColors.primary800,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(const Radius.circular(6).r)),
              child: InkWell(
                onTap: () {
                  ref.refresh(winOrLoseCheckboxProvider.notifier).state;
                  ref.refresh(longOrShortCheckboxProvider.notifier).state;
                  ref.refresh(showChartProvider.notifier).state;
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            BacktestStatisticsDetailScreen(currentIndex: index),
                      ));
                },
                child: Padding(
                  padding: const EdgeInsets.only(
                          left: 12, right: 12, top: 16, bottom: 12)
                      .dm,
                  child: Column(
                    children: [
                      4.verticalSpace,
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Text(
                              bs.title,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 14.sp),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              bs.accountSize.toStringAsFixed(2),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 14.sp),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Text(
                              bs.pair,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 14.sp),
                            ),
                          ),
                        ],
                      ),
                      4.h.verticalSpace,
                      const Divider(
                        color: Colors.grey,
                      ),
                      4.h.verticalSpace,
                      IntrinsicHeight(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Text(
                                      'Count',
                                      textAlign: TextAlign.left,
                                      style:
                                          TextStyle(color: AppColors.text100),
                                    ),
                                    Text(
                                      bs.statistics.isEmpty
                                          ? AppConsts.emptyValue
                                          : bs.statistics.length.toString(),
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: bs.statistics.isEmpty
                                              ? AppColors.text100
                                              : AppColors.text400,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                )),
                            const VerticalDivider(),
                            Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  Text(
                                    'Average RR',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: AppColors.text100),
                                  ),
                                  Text(
                                    result.averageRR!,
                                    style: TextStyle(
                                        fontSize: 14.sp,
                                        color: result.averageRR ==
                                                AppConsts.emptyValue
                                            ? AppColors.text100
                                            : AppColors.text400,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            ),
                            const VerticalDivider(),
                            Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Text(
                                      'Profit',
                                      style:
                                          TextStyle(color: AppColors.text100),
                                    ),
                                    Text(
                                      result.totalProfit!,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: result.totalProfit ==
                                                  AppConsts.emptyValue
                                              ? AppColors.text100
                                              : AppColors.text400,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                )),
                          ],
                        ),
                      ),
                      6.h.verticalSpace,
                      IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                                flex: 1,
                                child: Column(
                                  children: [
                                    Text(
                                      'Win Rate',
                                      textAlign: TextAlign.left,
                                      style:
                                          TextStyle(color: AppColors.text100),
                                    ),
                                    Text(
                                      result.winRate!,
                                      style: TextStyle(
                                          fontSize: 14.sp,
                                          color: result.winRate ==
                                                  AppConsts.emptyValue
                                              ? AppColors.text100
                                              : AppColors.text400,
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                )),
                            const VerticalDivider(),
                            Expanded(
                              flex: 1,
                              child: bs.statistics.isEmpty
                                  ? Column(
                                      children: [
                                        Text(
                                          'Win / Loss',
                                          style: TextStyle(
                                              color: AppColors.text100),
                                        ),
                                        Text(
                                          '-----  :  -----',
                                          style: TextStyle(
                                              fontSize: 14.sp,
                                              color: AppColors.text100,
                                              fontWeight: FontWeight.bold),
                                        )
                                      ],
                                    )
                                  : Column(
                                      children: [
                                        Text(
                                          'Win / Loss',
                                          style: TextStyle(
                                              color: AppColors.text100),
                                        ),
                                        Text.rich(
                                          TextSpan(
                                            children: [
                                              TextSpan(
                                                  text: '${result.winCount}',
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              const TextSpan(text: ' : '),
                                              TextSpan(
                                                  text: '${result.lossCount}',
                                                  style: TextStyle(
                                                      color: Colors.red,
                                                      fontSize: 14.sp,
                                                      fontWeight:
                                                          FontWeight.bold))
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        Positioned(
          right: -2.w,
          top: 12.h,
          child: PopupMenuButton<int>(
            // iconColor: Colors.black87,
            position: PopupMenuPosition.over,
            onSelected: (int item) async {
              switch (item) {
                case 1:
                  showDialog(
                    context: context,
                    builder: (context) => AddTitleDialog(
                      currentIndex: index,
                      titleStr: bs.title,
                      accountSizeStr: bs.accountSize.toString(),
                      pairStr: bs.pair,
                    ),
                  );
                  break;
                case 2:
                  showDialog(
                    context: context,
                    builder: (context) => const DeleteConfirmDialog(),
                  ).then((value) async {
                    if (value) {
                      ref
                          .read(backtestStatisticsListProvider.notifier)
                          .state
                          .removeAt(index);
                      ref.read(backtestStatisticsListProvider.notifier).state =
                          [...ref.read(backtestStatisticsListProvider)];
                      final prefUtils =
                          ref.read(preferenceUtils.notifier).state;
                      await prefUtils.saveStatisticsStrList(
                        BacktestStatistics.toJsonList(ref
                            .read(backtestStatisticsListProvider.notifier)
                            .state),
                      );
                    }
                  });
                  break;
                default:
              }
            },
            itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
              const PopupMenuItem<int>(
                value: 1,
                child: Text('Edit'),
              ),
              const PopupMenuItem<int>(
                value: 2,
                child: Text('Delete'),
              ),
            ],
          ),
        )
      ],
    );
  }
}
