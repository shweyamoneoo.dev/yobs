import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:yobs/model/backtest_statistics.dart';
import 'package:yobs/utils/app_consts.dart';

import '../providers/common_providers.dart';
import '../utils/app_colors.dart';

class AddTitleDialog extends HookConsumerWidget {
  final String? titleStr;
  final String? accountSizeStr;
  final String? pairStr;
  final int? currentIndex;
  const AddTitleDialog(
      {this.titleStr = '',
      this.accountSizeStr = '',
      this.pairStr = '',
      this.currentIndex,
      super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    TextEditingController title = useTextEditingController(text: titleStr);
    TextEditingController accountSize =
        useTextEditingController(text: accountSizeStr);
    TextEditingController pair = useTextEditingController(text: pairStr);
    return Dialog(
      backgroundColor: AppColors.primary900,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0).r), //this right here
      child: SizedBox(
        height: 0.36.sh,
        child: Padding(
          padding: const EdgeInsets.only(
                  bottom: 8, top: 16.0, left: 16.0, right: 16.0)
              .dm,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextField(
                controller: title,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                    contentPadding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 4)
                            .dg,
                    border: const OutlineInputBorder(),
                    hintText: 'Strategy Name'),
              ),
              4.verticalSpace,
              TextField(
                controller: accountSize,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(AppConsts.doubleOnly)
                ],
                decoration: InputDecoration(
                    contentPadding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 4)
                            .dg,
                    border: const OutlineInputBorder(),
                    hintText: 'Account Size'),
              ),
              4.verticalSpace,
              TextField(
                controller: pair,
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                    contentPadding:
                        const EdgeInsets.symmetric(horizontal: 8, vertical: 4)
                            .dg,
                    border: const OutlineInputBorder(),
                    hintText: 'Pair'),
              ),
              6.verticalSpace,
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      "Cancel",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  4.horizontalSpace,
                  ElevatedButton(
                    onPressed: () async {
                      if (title.text.isEmpty ||
                          accountSize.text.isEmpty ||
                          pair.text.isEmpty) {
                        return;
                      }
                      if (currentIndex == null) {
                        int index = ref
                            .read(backtestStatisticsListProvider.notifier)
                            .state
                            .length;
                        ref
                            .read(backtestStatisticsListProvider.notifier)
                            .state = [
                          ...ref
                              .read(backtestStatisticsListProvider.notifier)
                              .state,
                          BacktestStatistics(
                              id: index,
                              title: title.text,
                              accountSize: double.parse(accountSize.text),
                              pair: pair.text,
                              statistics: []),
                        ];
                      } else {
                        ref
                                .read(backtestStatisticsListProvider.notifier)
                                .state[currentIndex!] =
                            BacktestStatistics(
                                id: currentIndex!,
                                title: title.text,
                                accountSize: double.parse(accountSize.text),
                                pair: pair.text,
                                statistics: ref
                                    .read(
                                        backtestStatisticsListProvider.notifier)
                                    .state[currentIndex!]
                                    .statistics);
                        ref
                            .read(backtestStatisticsListProvider.notifier)
                            .state = [
                          ...ref
                              .read(backtestStatisticsListProvider.notifier)
                              .state
                        ];
                      }

                      final prefUtils =
                          ref.read(preferenceUtils.notifier).state;
                      await prefUtils.saveStatisticsStrList(
                          BacktestStatistics.toJsonList(ref
                              .read(backtestStatisticsListProvider.notifier)
                              .state));
                      if (context.mounted) {
                        Navigator.pop(context);
                      }
                    },
                    child: const Text(
                      "Add",
                      // style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
