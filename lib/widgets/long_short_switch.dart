import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LongShortSwitch extends StatelessWidget {
  final bool isLong;
  final Function onTap;
  const LongShortSwitch({super.key, required this.isLong, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap as void Function(),
      child: Container(
        width: 0.26.sw,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8).r,
            // border: Border.all(color: isAWin ? Colors.green : Colors.redAccent),
            color: isLong
                ? Colors.blueAccent.shade100
                : Colors.deepPurpleAccent.shade100),
        child: Padding(
          padding: const EdgeInsets.all(3.0).dg,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 0.11.sw,
                height: 20.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8).r,
                    color: isLong
                        ? Colors.blueAccent
                        : Colors.deepPurpleAccent.shade100),
                child: Center(
                    child: Text(
                  'Long',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: isLong ? Colors.white : Colors.white70,
                    fontSize: 12.sp,
                  ),
                )),
              ),
              Container(
                width: 0.11.sw,
                height: 20.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8).r,
                    color: isLong
                        ? Colors.blueAccent.shade100
                        : Colors.deepPurple),
                child: Center(
                    child: Text(
                  'Short',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: !isLong ? Colors.white : Colors.white70,
                    fontSize: 12.sp,
                  ),
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
