import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:yobs/model/single_statistics.dart';
import 'package:yobs/widgets/delete_confirm_dialog.dart';
import 'package:yobs/widgets/long_short_switch.dart';
import 'package:yobs/widgets/win_loss_switch.dart';

import '../model/backtest_statistics.dart';
import '../providers/common_providers.dart';
import '../utils/app_consts.dart';

class StatisticsListItem extends HookConsumerWidget {
  final SingleStatistics statistics;
  final int stIndex;
  final int index;
  const StatisticsListItem(
      {required this.statistics,
      required this.stIndex,
      required this.index,
      super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final amount = useTextEditingController(text: statistics.amount.toString());
    final rr = useTextEditingController(text: statistics.singleRR.toString());

    return Padding(
      padding: const EdgeInsets.only(top: 8.0).dg,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          DecoratedBox(
              decoration:
                  BoxDecoration(shape: BoxShape.circle, border: Border.all()),
              child: Padding(
                padding: const EdgeInsets.all(5.0).dm,
                child: Text('${stIndex + 1}'),
              )),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                IntrinsicHeight(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Expanded(
                        child: TextField(
                          textAlign: TextAlign.center,
                          controller: amount,
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                AppConsts.doubleOnly)
                          ],
                          onTapOutside: (event) =>
                              FocusManager.instance.primaryFocus?.unfocus(),
                          onSubmitted: (value) {
                            if (value.isNotEmpty) {
                              editStatistics(
                                  ref,
                                  index,
                                  stIndex,
                                  statistics.isLong,
                                  amount.text,
                                  rr.text,
                                  statistics.isAWin);
                            } else {
                              amount.text = statistics.amount.toString();
                            }
                          },
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none),
                              focusedBorder: OutlineInputBorder(),
                              suffix: Icon(
                                Icons.percent_outlined,
                                size: 16,
                              )),
                        ),
                      ),
                      const VerticalDivider(),
                      Expanded(
                        child: TextField(
                          textAlign: TextAlign.center,
                          controller: rr,
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(
                                AppConsts.doubleOnly)
                          ],
                          onTapOutside: (event) =>
                              FocusManager.instance.primaryFocus?.unfocus(),
                          onSubmitted: (value) {
                            if (value.isNotEmpty) {
                              editStatistics(
                                  ref,
                                  index,
                                  stIndex,
                                  statistics.isLong,
                                  amount.text,
                                  rr.text,
                                  statistics.isAWin);
                            } else {
                              rr.text = statistics.singleRR.toString();
                            }
                          },
                          decoration: const InputDecoration(
                            border:
                                OutlineInputBorder(borderSide: BorderSide.none),
                            focusedBorder: OutlineInputBorder(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                4.verticalSpace,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    0.05.sw.horizontalSpace,
                    Expanded(
                      child: LongShortSwitch(
                          isLong: statistics.isLong,
                          onTap: () {
                            editStatistics(
                                ref,
                                index,
                                stIndex,
                                !statistics.isLong,
                                amount.text,
                                rr.text,
                                statistics.isAWin);
                          }),
                    ),
                    0.07.sw.horizontalSpace,
                    Expanded(
                      child: WinLossSwitch(
                        isAWin: statistics.isAWin,
                        onTap: () {
                          editStatistics(ref, index, stIndex, statistics.isLong,
                              amount.text, rr.text, !statistics.isAWin);
                        },
                      ),
                    ),
                    0.05.sw.horizontalSpace,
                  ],
                ),
              ],
            ),
          ),
          6.horizontalSpace,
          IconButton(
            onPressed: () async {
              showDialog(
                context: context,
                builder: (context) => const DeleteConfirmDialog(),
              ).then((value) {
                if (value) {
                  deleteStatistics(ref, index);
                }
              });
            },
            icon: const Icon(
              Icons.delete_outline,
            ),
          ),
        ],
      ),
    );
  }

  void deleteStatistics(WidgetRef ref, int index) async {
    ref
        .read(backtestStatisticsListProvider.notifier)
        .state[index]
        .statistics
        .removeAt(stIndex);
    ref.read(backtestStatisticsListProvider.notifier).state = [
      ...ref.read(backtestStatisticsListProvider.notifier).state
    ];
    final prefUtils = ref.read(preferenceUtils.notifier).state;
    await prefUtils.saveStatisticsStrList(BacktestStatistics.toJsonList(
        ref.read(backtestStatisticsListProvider.notifier).state));
  }

  void editStatistics(WidgetRef ref, int index, int stIndex, bool isLong,
      String amount, String rr, bool isAWin) async {
    if (amount.isEmpty || rr.isEmpty) {
      return;
    }
    ref
            .read(backtestStatisticsListProvider.notifier)
            .state[index]
            .statistics[stIndex] =
        SingleStatistics(
            isLong: isLong,
            amount: double.parse(amount),
            singleRR: double.parse(rr),
            isAWin: isAWin);
    ref.read(backtestStatisticsListProvider.notifier).state = [
      ...ref.read(backtestStatisticsListProvider.notifier).state
    ];
    final prefUtils = ref.read(preferenceUtils.notifier).state;
    await prefUtils.saveStatisticsStrList(BacktestStatistics.toJsonList(
        ref.read(backtestStatisticsListProvider.notifier).state));
  }
}
