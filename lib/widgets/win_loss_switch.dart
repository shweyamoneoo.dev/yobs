import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WinLossSwitch extends StatelessWidget {
  final bool isAWin;
  final Function onTap;
  const WinLossSwitch({super.key, required this.isAWin, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap as void Function(),
      child: Container(
        width: 0.26.sw,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            // border: Border.all(color: isAWin ? Colors.green : Colors.redAccent),
            color: isAWin ? const Color(0xFF3b625c) : const Color(0xFF613748)),
        child: Padding(
          padding: const EdgeInsets.all(3.0).dg,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 0.11.sw,
                height: 20.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: isAWin ? Colors.green : const Color(0xFF613748)),
                child: Center(
                    child: Text(
                  'Win',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 12.sp,
                  ),
                )),
              ),
              Container(
                width: 0.11.sw,
                height: 20.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: isAWin ? const Color(0xFF3b625c) : Colors.red[400]),
                child: Center(
                    child: Text(
                  'Loss',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 12.sp,
                  ),
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
