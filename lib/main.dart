import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:yobs/providers/common_providers.dart';
import 'package:yobs/screens/home.dart';
import 'package:yobs/utils/theme_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final SharedPreferences prefs = await SharedPreferences.getInstance();

  runApp(ProviderScope(
    overrides: [
      sharedPreferencesProvider.overrideWithValue(prefs),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // final base64Encoder = base64.encoder;
    // const sample = 'Dart is open source';
    // final encodedSample = base64Encoder.convert(sample.codeUnits);
    // print(encodedSample);
    // const plainText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';

    // final key = Key.fromSecureRandom(32);
    // final iv = IV.fromSecureRandom(16);
    // final encrypter = Encrypter(AES(key));

    // final encrypted = encrypter.encrypt(plainText, iv: iv);
    // final decrypted = encrypter.decrypt(encrypted, iv: iv);

    // print(decrypted);
    return ScreenUtilInit(
      minTextAdapt: true,
      splitScreenMode: true,
      // Use builder only if you need to use library outside ScreenUtilInit context
      builder: (_, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Your Own Backtesting Statistics',
          theme: themeData(ThemeData.dark()),
          home: const HomeScreen(),
        );
      },
    );
  }
}
