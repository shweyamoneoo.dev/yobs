import 'package:shared_preferences/shared_preferences.dart';

class PreferenceUtils {
  final SharedPreferences sharedPreferences;
  PreferenceUtils({required this.sharedPreferences});

  String key = 'LIST';

  Future<void> saveStatisticsStrList(List<String> list) async {
    await sharedPreferences.setStringList(key, list);
  }

  List<String>? getStatisticsStrList() {
    return sharedPreferences.getStringList(key) ?? [];
  }
}
