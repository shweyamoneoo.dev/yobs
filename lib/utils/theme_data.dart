import 'package:flutter/material.dart';
import 'package:yobs/utils/app_colors.dart';

ThemeData themeData(ThemeData baseTheme) {
  // Dark Theme
  return baseTheme.copyWith(
    primaryColor: AppColors.text50,
    primaryColorDark: Colors.black,
    primaryColorLight: AppColors.primary100,
    scaffoldBackgroundColor: AppColors.primary900,
    textTheme: TextTheme(
      displayLarge: TextStyle(
        color: AppColors.text200,
      ),
      displayMedium: TextStyle(
        color: AppColors.text200,
      ),
      displaySmall: TextStyle(
        color: AppColors.text200,
      ),
      headlineMedium: TextStyle(
        color: AppColors.text200,
      ),
      headlineSmall: TextStyle(
        color: AppColors.text200,
        fontWeight: FontWeight.bold,
      ),
      titleLarge: TextStyle(
        color: AppColors.text200,
      ),
      titleMedium: TextStyle(
        color: AppColors.text200,
        fontWeight: FontWeight.bold,
      ),
      titleSmall: TextStyle(
        color: AppColors.text200,
        fontWeight: FontWeight.bold,
      ),
      bodyLarge: TextStyle(
        color: AppColors.text200,
      ),
      bodyMedium: TextStyle(
        color: AppColors.text200,
        fontWeight: FontWeight.bold,
      ),
      labelLarge: TextStyle(
        color: AppColors.text200,
      ),
      bodySmall: TextStyle(
        color: AppColors.text200,
      ),
      labelSmall: TextStyle(
        color: AppColors.text200,
      ),
    ),
    iconTheme: IconThemeData(
      color: AppColors.primary300,
    ),
    appBarTheme: AppBarTheme(
      color: AppColors.primary800,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        iconColor: WidgetStateProperty.all(Colors.black87),
        backgroundColor: WidgetStateProperty.all(AppColors.primary700),
        foregroundColor: WidgetStateProperty.all(Colors.white),
        padding: WidgetStateProperty.all(
            const EdgeInsets.symmetric(vertical: 8, horizontal: 16)),
        shape: WidgetStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
      ),
    ),
  );
}
