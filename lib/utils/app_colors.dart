import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  ///
  /// Primary Colors
  ///
  static Color primary50 = const Color(0xFFe6ecf1);
  static Color primary100 = const Color(0xFFc3cfd7);
  static Color primary200 = const Color(0xFF9fafba);
  static Color primary300 = const Color(0xFF7b909e);
  static Color primary400 = const Color(0xFF61798a);
  static Color primary500 = const Color(0xFF476476);
  static Color primary600 = const Color(0xFF3b5667);
  static Color primary700 = const Color(0xFF2d4452);
  static Color primary800 = const Color(0xFF20323d);
  static Color primary900 = const Color(0xFF101e27);

  ///
  /// text Colors
  ///
  static Color text50 = const Color(0xFFfef9e3);
  static Color text100 = const Color(0xFFfdf0b8);
  static Color text200 = const Color(0xFFfde68a);
  static Color text300 = const Color(0xFFfddd5a);
  static Color text400 = const Color(0xFFfdd437);
  static Color text500 = const Color(0xFFfecc22);
  static Color text600 = const Color(0xFFfebe1a);
  static Color text700 = const Color(0xFFfeab14);
  static Color text800 = const Color(0xFFff9b0e);
  static Color text900 = const Color(0xFFff7c02);
}
