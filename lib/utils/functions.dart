import 'package:fl_chart/fl_chart.dart';
import 'package:yobs/model/backtest_statistics.dart';
import 'package:yobs/model/single_statistics.dart';

import '../model/backtest_result.dart';
import 'app_consts.dart';

BacktestResult processStatistics(BacktestStatistics bs) {
  BacktestResult result = BacktestResult();
  result.count = bs.statistics.length;
  if (result.count == 0) {
    return BacktestResult(
        count: 0,
        averageRR: AppConsts.emptyValue,
        largestProfit: 0,
        lossCount: 0,
        totalProfit: AppConsts.emptyValue,
        winCount: 0,
        winRate: AppConsts.emptyValue);
  }

  double totalRR = 0;
  int winCount = 0;
  int lossCount = 0;
  double totalProfit = 0;
  double largestProfit = 0;
  for (SingleStatistics s in bs.statistics) {
    if (s.isAWin) {
      totalRR += s.singleRR;
      winCount += 1;
      totalProfit += profit(bs.accountSize, s);
    } else {
      totalRR -= 1;
      lossCount += 1;
      totalProfit -= profit(bs.accountSize, s);
    }
    if (totalProfit < 0) totalProfit = 0;
    if (largestProfit < totalProfit) {
      largestProfit = totalProfit;
    }
  }
  result.averageRR = (totalRR / result.count!).toStringAsFixed(2);
  result.winRate = ((100 / bs.statistics.length) * winCount).toStringAsFixed(2);
  result.largestProfit = largestProfit;
  result.totalProfit = totalProfit.toStringAsFixed(2);
  result.winCount = winCount;
  result.lossCount = lossCount;

  return result;
}

// String averageRR(BacktestStatistics bs) {
//   if (bs.statistics.isNotEmpty) {
//     double total = 0;
//     for (SingleStatistics s in bs.statistics) {
//       if (s.isAWin) {
//         total += s.singleRR;
//       }
//     }
//     return (total / bs.statistics.length).toStringAsFixed(2);
//   }
//   return AppConsts.emptyValue;
// }

// String winRate(BacktestStatistics bs) {
//   if (bs.statistics.isNotEmpty) {
//     return ((100 / bs.statistics.length) * winCount(bs)).toStringAsFixed(2);
//   } else {
//     return AppConsts.emptyValue;
//   }
// }

// String totalProfit(BacktestStatistics bs) {
//   if (bs.statistics.isNotEmpty) {
//     double total = 0;
//     for (SingleStatistics s in bs.statistics) {
//       double p = profit(bs.accountSize, s);
//       if (s.isAWin) {
//         total += p;
//       } else {
//         total -= p;
//       }
//       if (total < 0) total = 0;
//     }
//     return total.toStringAsFixed(2);
//   } else {
//     return AppConsts.emptyValue;
//   }
// }

double largestProfit(BacktestStatistics bs) {
  double largestProfit = 0;
  if (bs.statistics.isNotEmpty) {
    double total = 0;
    for (SingleStatistics s in bs.statistics) {
      double p = profit(bs.accountSize, s);
      if (s.isAWin) {
        total += p;
      } else {
        total -= p;
      }
      if (total < 0) total = 0;
      if (largestProfit < total) {
        largestProfit = total;
      }
    }
    return largestProfit;
  } else {
    return 0;
  }
}

List<FlSpot> profitAddingList(BacktestStatistics bs, num divideBy) {
  List<FlSpot> result = [const FlSpot(0, 0)];
  double count = 1;
  double currentTotal = 0;
  for (SingleStatistics s in bs.statistics) {
    double p = profit(bs.accountSize, s);
    if (s.isAWin) {
      currentTotal += p;
    } else {
      currentTotal -= p;
    }
    if (currentTotal < 0) currentTotal = 0;
    result.add(FlSpot(count++, currentTotal / divideBy));
    // result.add(FlSpot(
    //     count++, double.parse((currentTotal / divideBy).toStringAsFixed(2))));
  }
  return result;
}

// int winCount(BacktestStatistics bs) {
//   if (bs.statistics.isNotEmpty) {
//     int winCount = 0;
//     for (SingleStatistics s in bs.statistics) {
//       if (s.isAWin) ++winCount;
//     }
//     return winCount;
//   }
//   return 0;
// }

double profit(double accountSize, SingleStatistics s, {int? divideBy}) {
  double result = (accountSize / 100) * s.amount;
  if (s.isAWin) {
    result *= s.singleRR;
  }
  if (divideBy != null) {
    return double.parse((result / divideBy).toStringAsFixed(2));
  }
  return double.parse(result.toStringAsFixed(2));
}
