class AppConsts {
  static RegExp doubleOnly = RegExp(r'^(\d+)?\.?\d{0,2}');
  static String emptyValue = '-----';
}